/**
 * Created by josoder on 01.09.17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {Navbar, NavItem, MenuItem, Nav, NavDropdown} from 'react-bootstrap';

class NavComponent extends React.Component {
    render() {
        return(
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/">Anxaid</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>
                    <NavItem eventKey={1} href="/quotes">Quotes</NavItem>
                    <NavItem eventKey={2} href="/register">Register</NavItem>
                    <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                        <MenuItem eventKey={3.1}>Action</MenuItem>
                        <MenuItem eventKey={3.2}>Another action</MenuItem>
                        <MenuItem eventKey={3.3}>Something else here</MenuItem>
                        <MenuItem divider />
                        <MenuItem eventKey={3.4}>Separated link</MenuItem>
                    </NavDropdown>
                </Nav>
            </Navbar>
        );
    }
}
export default NavComponent;
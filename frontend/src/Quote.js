import React, {Component} from 'react';
import './App.css';

class Quotes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quotes: []
        };
    }

    componentDidMount() {
        fetch('/quotes')
            .then(response => response.json())
            .then(quotes => this.setState({quotes: quotes}))
            .catch(err => document.write(err));
    }

    render() {
        return (
            <div className="container">
                <QuoteList quotes={this.state.quotes}/>
            </div>
        );
    }
}

class QuoteList extends React.Component {
    render() {
        var quotes = this.props.quotes.map(quote =>
            <Quote quote={quote}/>
    );
        ;
        return (
            <table>
                <tbody>
                <tr>
                    <th>Author</th>
                    <th>Quote Text</th>
                    <th>Description</th>
                    <th>Category</th>
                </tr>
                {quotes}
                </tbody>
            </table>
        )
    }
}

class Quote extends React.Component {
    render() {
        return(
        <tr>
            <td>{this.props.quote.author}</td>
            <td>{this.props.quote.quoteText}</td>
            <td>{this.props.quote.description}</td>
            <td>{this.props.quote.category}</td>
        </tr>
        );
    }
}

export default Quotes;

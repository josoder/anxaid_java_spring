/**
 * Created by josoder on 31.08.17.
 */
import React, {Component} from 'react';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            confirmPassword: '',
            email: ''
        };
    }

    render() {
        return (
            <div className="container">
                <div className="form-group">
                    <label htmlFor="input-username">
                        Username
                    </label>
                    <input
                        id="input-username"
                        className="form-control"
                        placeholder="Username"
                        onChange={e => {
                            this.setState(
                                {username: e.target.value});
                        }}/>

                </div>

                <div className="form-group">
                    <label htmlFor="input-password">
                        Password
                    </label>
                    <input
                        id="input-password"
                        className="form-control"
                        placeholder="Password"
                        type="password"
                        onChange={e => {
                            this.setState(
                                {password: e.target.value});
                        }}/>

                </div>

                <div className="form-group">
                    <label htmlFor="confirm-password">
                        Confirm Password
                    </label>
                    <input
                        id="confirm-password"
                        className="form-control"
                        placeholder="Confirm Password"
                        type="password"
                        onChange={e => {
                            this.setState(
                                {confirmPassword: e.target.value});
                        }}/>

                </div>

                <div className="form-group">
                    <label htmlFor="input-email">
                        Email
                    </label>
                    <input
                        id="email"
                        className="form-control"
                        placeholder="Email"
                        onChange={e => {
                            this.setState(
                                {email: e.target.value});
                        }}/>

                </div>

                <div className="form-group  ">
                    <button
                        className="btn btn-primary btn-block"
                        onClick={() => {
                            const username = this.state.username;
                            const email = this.state.email;
                            const password = this.state.password;
                            const confirm = this.state.confirmPassword;
                            if (username.length==0){
                                alert('you need to provide a valid username');
                            }
                            if (email.length==0){
                                alert('you need to provide a valid email');
                            }
                            if(password!=confirm || password.length==0){
                                alert('passwords does not match or is invalid');
                            } else {
                                alert('user is valid');
                            }
                        }}
                    >
                        OK
                    </button>
                </div>
            </div>
    );

    }
}

export default Register;
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Quotes from './Quote';
import {Router, Route, browserHistory} from 'react-router';
import NavComponent from './Navbar';
import Register from './Register'
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

class Home extends React.Component {
    render() {
        return(
            <div className="container">
            <h2>Welcome to Anxaid</h2>
            </div>
        );
    }
}

ReactDOM.render(
   <Router history={browserHistory}>
       <Route path="/" component={Home}/>
       <Route path="/register" component={Register}/>
       <Route path="/quotes" component={Quotes}/>
   </Router>,
    document.getElementById('root')
);

ReactDOM.render(<NavComponent/>, document.getElementById("nav_top"));
registerServiceWorker();

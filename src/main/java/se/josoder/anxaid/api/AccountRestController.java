package se.josoder.anxaid.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.josoder.anxaid.model.account.Account;
import se.josoder.anxaid.repository.AccountRepository;

import javax.validation.Valid;
import java.net.URI;

/**
 * Created by josoder on 30.07.17.
 */
@RestController
public class AccountRestController {
    @Autowired
    private AccountRepository accountRepository;


    @GetMapping("/accounts")
    public Iterable<Account> retrieveAccounts() {
        return accountRepository.findAll();
    }

    @GetMapping("/accounts/{accountId}")
    public Account retrieveAccount(@PathVariable long accountId){
        return accountRepository.findOne(accountId);
    }

    @PostMapping("/accounts")
    public ResponseEntity<Void> addAccount(@Valid @RequestBody Account newAccount){
        Account account = accountRepository.save(newAccount);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(account.getId()).toUri();
        return ResponseEntity.created(location).build();
    }
}

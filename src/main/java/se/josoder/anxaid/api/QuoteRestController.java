package se.josoder.anxaid.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.josoder.anxaid.model.quote.Quote;
import se.josoder.anxaid.service.QuoteService;

import javax.validation.Valid;
import java.net.URI;

/**
 * Created by josoder on 23.07.17.
 */
@RestController
public class QuoteRestController {
    @Autowired
    private QuoteService service;

    @GetMapping("/quotes")
    public Iterable<Quote> getAllQuotes(){
        return service.getAllQuotes();
    }

    @GetMapping("quotes/{quoteID}")
    public Quote getQuoteById(@PathVariable Long quoteID){
        return service.getQuoteById(quoteID);
    }

    @PostMapping("/quotes")
    public ResponseEntity<Void> addQuote(@Valid @RequestBody Quote newQuote){
        Quote quote = service.addQuote(newQuote);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(quote.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/quotes/{quoteID}")
    public ResponseEntity<Quote> updateQuoteById(@PathVariable Long quoteID ,@Valid @RequestBody Quote updatedQuote){
        Quote quote = service.getQuoteById(quoteID);

        if(quote == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        quote = service.updateQuote(quoteID, updatedQuote);
        return new ResponseEntity<>(quote, HttpStatus.OK);
    }

    @DeleteMapping("/quotes/{quoteID}")
    public ResponseEntity<Void> deleteQuoteById(@PathVariable Long quoteID){
        Quote quote = service.getQuoteById(quoteID);
        if(quote == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        service.deleteQuote(quoteID);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}

package se.josoder.anxaid.api;

import org.aspectj.weaver.ast.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.josoder.anxaid.model.therapy.test.Situation;
import se.josoder.anxaid.model.therapy.test.TherapyTest;
import se.josoder.anxaid.service.TherapyTestService;

import javax.validation.Valid;
import java.net.URI;

/**
 * Created by josoder on 08.08.17.
 */
@RestController
public class TherapyTestRestController {
    @Autowired
    private TherapyTestService testService;

    @GetMapping("/tests")
    public Iterable<TherapyTest> retrieveAllTests(){
        return testService.getAllTests();
    }

    @GetMapping("/tests/{anxietyType}")
    public TherapyTest retrieveTestById(@PathVariable String anxietyType) {
        return testService.getTestByAnxietyType(anxietyType);
    }

    @PostMapping("/tests")
    public ResponseEntity<Void> addTest(@Valid @RequestBody TherapyTest newTest) {
        TherapyTest test = testService.addTherapyTest(newTest);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{anxietyType}").buildAndExpand(test.getAnxietyType()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/tests/{anxietyType}")
    public ResponseEntity<TherapyTest> updateTest(@PathVariable String anxietyType, @Valid @RequestBody TherapyTest updatedTest){
        TherapyTest test = testService.getTestByAnxietyType(anxietyType);
        if(test == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        testService.updateTherapyTest(updatedTest);
        return new ResponseEntity<>(updatedTest, HttpStatus.OK);
    }

    @DeleteMapping("/tests/{anxietyType}")
    public ResponseEntity<TherapyTest> deleteTest(@PathVariable String anxietyType){
        TherapyTest test = testService.getTestByAnxietyType(anxietyType);
        if(test == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        testService.deleteTest(test);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/tests/{anxietyType}/situations")
    public Iterable<Situation> retrieveAllTestSituations(@PathVariable String anxietyType){
        return testService.getAllSituationsFromTest(anxietyType);
    }

    @PostMapping("tests/{anxietyType}/situations")
    public ResponseEntity<Void> addSituationToTest(@PathVariable String anxietyType,
                                                   @Valid @RequestBody Situation newSituation){
        Situation situation = testService.addSituationToTest(anxietyType, newSituation);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{situationId}").buildAndExpand(situation.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("tests/{anxietyType}/situations/{situationId}")
    public ResponseEntity<Situation> updateTestSituation(@PathVariable String anxietyType,
                                                         @PathVariable long situationId,
                                                         @RequestBody @Valid Situation updatedSituation){
        Situation situation = testService.getSituationFromTest(anxietyType, situationId);
        if(situation == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        testService.updateTestSituation(anxietyType, situation);
        return new ResponseEntity<>(updatedSituation, HttpStatus.OK);
    }

    @DeleteMapping("tests/{anxietyType}/situations/{situationId}")
    public ResponseEntity<Void> deleteTestSituation(@PathVariable String anxietyType,
                                                    @PathVariable long situationId){
        if (testService.getSituationFromTest(anxietyType, situationId)==null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        testService.deleteTestSituation(anxietyType, situationId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/tests/{anxietyType}/situations/{situationId}")
    public Situation retrieveSituationFromTest(@PathVariable String anxietyType,
                                               @PathVariable long situationId){
        return testService.getSituationFromTest(anxietyType, situationId);
    }
}

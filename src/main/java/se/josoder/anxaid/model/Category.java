package se.josoder.anxaid.model;

/**
 * Created by josoder on 21.07.17.
 */
public interface Category {
    public static final String GENERAL_ANXIETY = "General anxiety";
    public static final String SOCIAL_ANXIETY = "Social anxiety";
}
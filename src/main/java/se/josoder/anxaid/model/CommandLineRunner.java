package se.josoder.anxaid.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.josoder.anxaid.model.account.Account;
import se.josoder.anxaid.model.quote.Quote;
import se.josoder.anxaid.model.therapy.test.Situation;
import se.josoder.anxaid.model.therapy.test.TherapyTest;
import se.josoder.anxaid.repository.QuoteRepository;
import se.josoder.anxaid.repository.AccountRepository;
import se.josoder.anxaid.repository.RoleRepository;
import se.josoder.anxaid.repository.therapy.TestRepository;
import se.josoder.anxaid.service.AccountService;

/**
 * Created by josoder on 21.07.17.
 */
@Component
public class CommandLineRunner implements org.springframework.boot.CommandLineRunner {
    @Autowired
    private QuoteRepository quoteRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestRepository testRepository;

    @Override
    public void run(String... strings) throws Exception {
        quoteRepository.save(new Quote("Joakim Söderstrand", "This is fictional", "test", Category.GENERAL_ANXIETY));
        quoteRepository.save(new Quote("Joakim Söderstrand", "This is fictional too", "test", Category.GENERAL_ANXIETY));
        quoteRepository.save(new Quote("Joakim Söderstrand", "This is fictional as well", "test", Category.GENERAL_ANXIETY));
        quoteRepository.save(new Quote("Joakim Söderstrand", "This is fictional..", "test", Category.GENERAL_ANXIETY));
        accountRepository.save(new Account("josoder", "Joakim Söderstrad", "j.sod.fake.se"));


        TherapyTest test = new TherapyTest("test");
        test.addSituation(new Situation("testing situation"));
        testRepository.save(test);
        TherapyTest test2 = new TherapyTest("t2");
        testRepository.save(test2);
    }
}

package se.josoder.anxaid.model.quote;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

/**
 * Created by josoder on 21.07.17.
 */
@Entity
public class Quote {
    @Id
    @GeneratedValue
    private long id;
    @NotEmpty
    @Size(max = 100)
    private String author;
    @NotEmpty
    @Size(max = 1000)
    private String quoteText;
    @Size(min = 1, max = 200)
    private String description;
    @NotEmpty
    private String category;

    protected Quote() {
    }

    public Quote(String author, String quoteText, String description, String category) {
        this.author = author;
        this.quoteText = quoteText;
        this.description = description;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getQuoteText() {
        return quoteText;
    }

    public void setQuoteText(String quoteText) {
        this.quoteText = quoteText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Quote quote = (Quote) o;

        if (author != null ? !author.equals(quote.author) : quote.author != null) return false;
        if (quoteText != null ? !quoteText.equals(quote.quoteText) : quote.quoteText != null) return false;
        if (description != null ? !description.equals(quote.description) : quote.description != null) return false;
        return category != null ? category.equals(quote.category) : quote.category == null;
    }

    @Override
    public int hashCode() {
        int result = author != null ? author.hashCode() : 0;
        result = 31 * result + (quoteText != null ? quoteText.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        return result;
    }
}

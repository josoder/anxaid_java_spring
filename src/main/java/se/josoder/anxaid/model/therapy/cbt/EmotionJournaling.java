package se.josoder.anxaid.model.therapy.cbt;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

/**
 * Created by josoder on 05.08.17.
 */
@Entity
public class EmotionJournaling {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    // The situation, what happened that triggered this emotion?
    @Size(min = 10, max = 1000)
    private String situation;
    // What thoughts was triggered in this situation ?
    @Size(min = 10, max = 1000)
    private String automaticThought;
    // which emotions was present in this situation?
    private String emotion;
    @Size(min = 1, max =  10)
    private int emotionIntensity;
    // could there be that this situation could be interpreted differently, can you come up with any?
    @Size(min = 1, max = 1000)
    private String adaptiveAnswer;
    // evaluate how much you believe in your automatic vs your adapted answers.
    @Size(min = 1, max = 1000)
    private String result;

    protected EmotionJournaling() {
    }

    public EmotionJournaling(String situation, String automaticThought, String emotion, int emotionIntensity, String adaptiveAnswer, String result) {
        this.situation = situation;
        this.automaticThought = automaticThought;
        this.emotion = emotion;
        this.emotionIntensity = emotionIntensity;
        this.adaptiveAnswer = adaptiveAnswer;
        this.result = result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public String getAutomaticThought() {
        return automaticThought;
    }

    public void setAutomaticThought(String automaticThought) {
        this.automaticThought = automaticThought;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public int getEmotionIntensity() {
        return emotionIntensity;
    }

    public void setEmotionIntensity(int emotionIntensity) {
        this.emotionIntensity = emotionIntensity;
    }

    public String getAdaptiveAnswer() {
        return adaptiveAnswer;
    }

    public void setAdaptiveAnswer(String adaptiveAnswer) {
        this.adaptiveAnswer = adaptiveAnswer;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}

package se.josoder.anxaid.model.therapy.test;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by josoder on 07.08.17.
 */
@Entity
public class Situation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Size(min = 5, max = 300)
    private String description;
    //private int[] anxietyLevels = new int[] {0,1,2,3};
    //private String[] anxietyLevelStrings = new String["none", "some", "moderate", "high"];
    @ManyToMany(mappedBy = "situations")
    @JsonIgnore
    private Set<TherapyTest> tests = new HashSet<TherapyTest>();

    protected Situation() {
    }

    public Situation(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<TherapyTest> getTests() {
        return tests;
    }

    public void setTests(Set<TherapyTest> tests) {
        this.tests = tests;
    }
}

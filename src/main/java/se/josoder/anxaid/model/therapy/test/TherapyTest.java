package se.josoder.anxaid.model.therapy.test;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by josoder on 07.08.17.
 */
@Entity
public class TherapyTest {
    @Id
    private String anxietyType;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "test_situation",
            joinColumns = @JoinColumn(name = "test_id"),
            inverseJoinColumns = @JoinColumn(name = "situation_id")
    )
    private Set<Situation> situations = new HashSet<Situation>();


    public TherapyTest(String anxietyType) {
        this.anxietyType = anxietyType;
    }

    protected TherapyTest() {
    }

    public String getAnxietyType() {
        return anxietyType;
    }

    public void setAnxietyType(String anxietyType) {
        this.anxietyType = anxietyType;
    }

    public void addSituation(Situation situation){
        this.situations.add(situation);
        situation.getTests().add(this);
    }

    public void removeSituation(Situation situation){
        this.situations.remove(situation);
        situation.getTests().remove(this);
    }

    public Set<Situation> getSituations() {
        return situations;
    }

    public void setSituations(Set<Situation> situations) {
        this.situations = situations;
    }

    // anxietyType has unique constraint so no need to compare anything else.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TherapyTest that = (TherapyTest) o;

        return anxietyType != null ? anxietyType.equals(that.anxietyType) : that.anxietyType == null;
    }

    @Override
    public int hashCode() {
        return anxietyType != null ? anxietyType.hashCode() : 0;
    }
}

package se.josoder.anxaid.repository;

import org.springframework.data.repository.CrudRepository;
import se.josoder.anxaid.model.account.Account;

/**
 * Created by josoder on 27.07.17.
 */
public interface AccountRepository extends CrudRepository<Account, Long> {
    public Account findByUsername(String username);
}

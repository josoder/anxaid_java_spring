package se.josoder.anxaid.repository;

import org.springframework.data.repository.CrudRepository;
import se.josoder.anxaid.model.quote.Quote;

/**
 * Created by josoder on 21.07.17.
 */
public interface QuoteRepository extends CrudRepository<Quote, Long> {
    public Iterable<Quote> findByAuthor(String author);

    public Quote findById(long id);
}

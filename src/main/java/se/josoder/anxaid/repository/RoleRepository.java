package se.josoder.anxaid.repository;

import org.springframework.data.repository.CrudRepository;
import se.josoder.anxaid.model.account.Role;

/**
 * Created by josoder on 02.08.17.
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
    public Role findByName(String name);
}

package se.josoder.anxaid.repository.therapy;

import org.springframework.data.repository.CrudRepository;
import se.josoder.anxaid.model.therapy.test.TherapyTest;

/**
 * Created by josoder on 08.08.17.
 */
public interface TestRepository extends CrudRepository<TherapyTest, Long> {
    TherapyTest findByAnxietyType(String anxietyType);
}

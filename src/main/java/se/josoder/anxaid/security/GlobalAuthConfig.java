package se.josoder.anxaid.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import se.josoder.anxaid.repository.AccountRepository;
import se.josoder.anxaid.model.account.Account;

import java.util.Arrays;

/**
 * Created by josoder on 01.08.17.
 */
@Configuration
public class GlobalAuthConfig extends GlobalAuthenticationConfigurerAdapter {
    @Autowired
    private AccountRepository accountRepository;

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
                Account account = accountRepository.findByUsername(s);

                if (account != null) {
                    GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(account.getRole().getName());
                    return new User(account.getUsername(), account.getPassword(), Arrays.asList(grantedAuthority));
                } else {
                    throw new UsernameNotFoundException("The account with username: " + account.getUsername() +
                            " could not be found..");
                }
            }
        };
    }

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        //auth.userDetailsService(userDetailsService());
        auth.inMemoryAuthentication().withUser("user1").password("secret1")
                .roles("USER").and().withUser("admin1").password("secret1")
                .roles("ADMIN");
    }
}

package se.josoder.anxaid.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se.josoder.anxaid.repository.AccountRepository;
import se.josoder.anxaid.repository.RoleRepository;
import se.josoder.anxaid.model.account.Account;

/**
 * Created by josoder on 02.08.17.
 */
@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoleRepository roleRepository;


    public void addAccount(Account account, String role) {
        //account.setPassword(passwordEncoder().encode(account.getPassword()));
        account.setRole(roleRepository.findByName(role));
        accountRepository.save(account);
    }

    @Bean
    private PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}

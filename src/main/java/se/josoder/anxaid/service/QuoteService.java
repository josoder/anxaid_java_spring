package se.josoder.anxaid.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.josoder.anxaid.model.quote.Quote;
import se.josoder.anxaid.repository.QuoteRepository;

/**
 * Created by josoder on 23.07.17.
 */
@Component
public class QuoteService {
    @Autowired
    private QuoteRepository repository;

    public Iterable<Quote> getAllQuotes(){
        return repository.findAll();
    }

    public Iterable<Quote> getQuoteByAuthor(String author){
        return repository.findByAuthor(author);
    }

    public Quote getQuoteById(Long id){
        return repository.findById(id);
    }

    public Quote addQuote(Quote quote){
        repository.save(quote);
        return quote;
    }

    public Quote updateQuote(Long quoteToUpdateId, Quote updatedQuote){
        /**
        Quote quote = repository.findById(quoteToUpdateId);
        quote.setAuthor(updatedQuote.getAuthor());
        quote.setCategory(updatedQuote.getCategory());
        quote.setDescription(updatedQuote.getDescription());
        quote.setQuoteText(updatedQuote.getQuoteText());
        repository.save(quote);
        **/
        updatedQuote.setId(quoteToUpdateId);
        repository.save(updatedQuote);
        return updatedQuote;
    }

    public void deleteQuote(Long id){
        repository.delete(id);
    }
}

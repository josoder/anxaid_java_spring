package se.josoder.anxaid.service;

import org.aspectj.weaver.ast.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.josoder.anxaid.model.therapy.test.Situation;
import se.josoder.anxaid.model.therapy.test.TherapyTest;
import se.josoder.anxaid.repository.therapy.TestRepository;

import java.util.Set;

/**
 * Created by josoder on 08.08.17.
 */
@Component
public class TherapyTestService {
    @Autowired
    private TestRepository testRepository;

    public Situation addSituationToTest(String anxietyType, Situation situation){
        TherapyTest test = testRepository.findByAnxietyType(anxietyType);
        if(test != null) {
            test.getSituations().add(situation);
            situation.getTests().add(test);
            testRepository.save(test);
            return situation;
        }
        return null;
    }

    public Situation updateTestSituation(String anxietyType, Situation situation) {
        TherapyTest test = testRepository.findByAnxietyType(anxietyType);
        test.addSituation(situation);
        testRepository.save(test);
        return situation;
    }

    public TherapyTest addTherapyTest(TherapyTest newTherapyTest){
        testRepository.save(newTherapyTest);
        return newTherapyTest;
    }

    public TherapyTest updateTherapyTest(TherapyTest updatedTherapyTest){
        testRepository.save(updatedTherapyTest);
        return updatedTherapyTest;
    }

    public Iterable<TherapyTest> getAllTests() {
        return testRepository.findAll();
    }

    public TherapyTest getTestByAnxietyType(String anxietyType){
        return testRepository.findByAnxietyType(anxietyType);
    }

    public void deleteTest(TherapyTest test) {
        testRepository.delete(test);
    }

    public Set<Situation> getAllSituationsFromTest(String anxietyType) {
        TherapyTest test = testRepository.findByAnxietyType(anxietyType);

        if(test != null) {
            return test.getSituations();
        }

        return null;
    }

    public void deleteTestSituation(String anxietyType, long situationId) {
        TherapyTest test = testRepository.findByAnxietyType(anxietyType);
        Situation situation = getSituationFromTest(anxietyType, situationId);
        test.getSituations().remove(getSituationFromTest(anxietyType, situationId));
        testRepository.save(test);
    }

    public Situation getSituationFromTest(String anxietyType, long situationId) {
        TherapyTest test = testRepository.findByAnxietyType(anxietyType);
        if(test != null) {
            for (Situation s : test.getSituations()) {
                if(s.getId() == situationId) return s;
            }
        }
        return null;
    }
}

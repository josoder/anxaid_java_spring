package se.josoder.anxaid.quote;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import se.josoder.anxaid.api.QuoteRestController;
import se.josoder.anxaid.model.Category;
import se.josoder.anxaid.model.quote.Quote;
import se.josoder.anxaid.service.QuoteService;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by josoder on 25.07.17.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = QuoteRestController.class, secure = false)
public class QuoteControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuoteService mockedService;

    private String getJsonFromObject(Quote quote){
        return "{\"author\": \"" + quote.getAuthor() + "\", " +
                "\"quoteText\":\"" + quote.getQuoteText() + "\"," +
                "\"description\":\"" + quote.getDescription() +"\"," +
                "\"category\":\"" + quote.getCategory() + "\"}";
    }

    @Test
    public void addQuote() throws Exception {
        Quote mockQuote = new Quote("author1", "test1", "test1", Category.GENERAL_ANXIETY);
        mockQuote.setId(1);

        when(mockedService.addQuote(any(Quote.class)))
                .thenReturn(mockQuote);
        String questionJson = "{\"author\":\"author1\",\"quoteText\":\"test1\",\"category\":\""+ Category.GENERAL_ANXIETY + "\"}";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/quotes")
                .accept(MediaType.APPLICATION_JSON)
                .content(questionJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals("http://localhost/quotes/1", response.getHeader(HttpHeaders.LOCATION));
    }

    @Test
    public void updateQuote() throws Exception {
        Quote mockedQuote = new Quote("Fake Author", "Fake Text", "Fake Description", Category.GENERAL_ANXIETY);
        mockedQuote.setId(1);
        Quote updatedQuote = new Quote("Author", "Text", "Description", Category.SOCIAL_ANXIETY);

        when(mockedService.getQuoteById(anyLong())).thenReturn(mockedQuote);
        when(mockedService.updateQuote(anyLong(), any(Quote.class))).thenReturn(updatedQuote);

        String questionJson = getJsonFromObject(updatedQuote);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/quotes/" + mockedQuote.getId())
                .accept(MediaType.APPLICATION_JSON)
                .content(questionJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void deleteQuote() throws Exception {
        Quote quoteToDelete = new Quote("Fake Author", "Fake Text", "Fake Description", Category.GENERAL_ANXIETY);
        quoteToDelete.setId(1);

        when(mockedService.getQuoteById(anyLong())).thenReturn(quoteToDelete);

        String questionJson = getJsonFromObject(quoteToDelete);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/quotes/" + quoteToDelete.getId())
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        assertTrue(response.getStatus() == HttpStatus.OK.value());
    }

    @Test
    public void getAllQuotes() throws Exception {
        List<Quote> mockList = Arrays.asList(
                new Quote("author1", "test1", "test1", Category.SOCIAL_ANXIETY),
                new Quote("author2", "test2", "test2", Category.GENERAL_ANXIETY));

        when(mockedService.getAllQuotes()).thenReturn(mockList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/quotes").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "[" +
                "{\"author\":\"author1\",\"description\":\"test1\"}," +
                "{\"author\":\"author2\",\"description\":\"test2\"}"
                + "]";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }

    @Test
    public void getDetailsForQuote() throws Exception {
        Quote mockQuote = new Quote("test author", "test", "test", Category.GENERAL_ANXIETY);
        when(mockedService.getQuoteById(anyLong())).thenReturn(mockQuote);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/quotes/4").accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        String expected = "{\"author\":\"test author\",\"quoteText\":\"test\"," +
                "\"description\":\"test\"}";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }
}

package se.josoder.anxaid.quote;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import se.josoder.anxaid.Application;
import se.josoder.anxaid.model.account.Account;
import se.josoder.anxaid.model.Category;
import se.josoder.anxaid.model.quote.Quote;
import se.josoder.anxaid.util.HttpHelper;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by josoder on 21.07.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QuoteRestControllerIT {
    private TestRestTemplate restTemplate;
    private HttpHeaders headers;

    @Before
    public void init(){
        restTemplate = new TestRestTemplate();
        headers = HttpHelper.createHttpHeaders("admin1", "secret1");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    }


    @LocalServerPort
    private int port;

    @Test
    public void testRetrieveAllQuotesWithoutAuth() throws Exception{
        String url = HttpHelper.getURL("/quote", port);

        // the "/quote/**" paths wont need auth
        HttpHeaders headersWithoutAuth = new HttpHeaders();

        ResponseEntity<List<Quote>> response = restTemplate.exchange(url,
                HttpMethod.GET, new HttpEntity<String>("DUMMY_DOESNT_MATTER",
                        headersWithoutAuth),
                new ParameterizedTypeReference<List<Quote>>() {
                });

        Quote quote = new Quote("Joakim Söderstrand", "This is fictional", "test", Category.GENERAL_ANXIETY);

       assertTrue(response.getBody().contains(quote));
    }

    @Test
    public void testAddQuote() {
        String url = HttpHelper.getURL("/quote", port);

        Quote quote = new Quote("Alan Watts", "just a test text", "test description",
                Category.GENERAL_ANXIETY);

        HttpEntity entity = new HttpEntity<Quote>(quote, headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

        String actual = response.getHeaders().get(HttpHeaders.LOCATION).get(0);
        assertTrue(actual.contains("/quote/"));
    }

    @Test
    public void testAddAccount() {
        String url = HttpHelper.getURL("/accounts", port);

        Account account = new Account("jthes", "josoderss", "j.s@fake.se");

        HttpEntity entity = new HttpEntity<Account>(account, headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

        String actual = response.getHeaders().get(HttpHeaders.LOCATION).get(0);
        assertTrue(actual.contains("/accounts"));
    }
}

package se.josoder.anxaid.therapy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import se.josoder.anxaid.api.TherapyTestRestController;
import se.josoder.anxaid.model.therapy.test.Situation;
import se.josoder.anxaid.model.therapy.test.TherapyTest;
import se.josoder.anxaid.service.TherapyTestService;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by josoder on 17.08.17.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = TherapyTestRestController.class, secure = false)
public class TherapyTestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TherapyTestService mockedService;

    @Test
    public void addTest() throws Exception {
        TherapyTest mockTest = new TherapyTest("testtype");

        when(mockedService.addTherapyTest(any(TherapyTest.class)))
                .thenReturn(mockTest);
        String questionJson = "{\"anxietyType\":\"testtype\"}";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/tests")
                .accept(MediaType.APPLICATION_JSON)
                .content(questionJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals("http://localhost/tests/testtype", response.getHeader(HttpHeaders.LOCATION));
    }

    @Test
    public void updateTest() throws Exception {
        TherapyTest testToUpdate = new TherapyTest("Test Type");
        TherapyTest updatedTest = new TherapyTest("Updated Type");

        when(mockedService.getTestByAnxietyType(anyString())).thenReturn(testToUpdate);
        when(mockedService.updateTherapyTest(any(TherapyTest.class))).thenReturn(updatedTest);

        String questionJson = getJsonFromTestObject(updatedTest);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/tests/"+ testToUpdate.getAnxietyType())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(questionJson);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();
        String expected = getJsonFromTestObject(updatedTest);
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        JSONAssert.assertEquals(expected, response.getContentAsString(), false);
    }

    @Test
    public void deleteTest() throws Exception {
        TherapyTest testToDelete = new TherapyTest("Test-type");

        when(mockedService.getTestByAnxietyType(anyString())).thenReturn(testToDelete);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/tests/Test-type");

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void getAllTests() throws Exception {
        List<TherapyTest> mockTestList = Arrays.asList(
                new TherapyTest("test1"),
                new TherapyTest("test2"));

        when(mockedService.getAllTests()).thenReturn(mockTestList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/tests").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "[" +
                "{\"anxietyType\":\"test1\",\"situations\":[]}," +
                "{\"anxietyType\":\"test2\",\"situations\":[]}"
                + "]";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }

    @Test
    public void getTestByAnxietyType() throws Exception {
        TherapyTest mockedTherapyTest = new TherapyTest("anxType");

        when(mockedService.getTestByAnxietyType(any(String.class))).thenReturn(mockedTherapyTest);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/tests/anxType")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "{\"anxietyType\":\"anxType\"}";

        // Strict = false, will be more like "contains" in String.class.
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }

    @Test
    public void getSituationsFromTest() throws Exception {
        HashSet<Situation> mockedSituations = new HashSet<>(Arrays.asList(
                new Situation("fake1"),
                new Situation("fake2")
        ));

        when(mockedService.getAllSituationsFromTest(any(String.class))).thenReturn(mockedSituations);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/tests/anxType/situations")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "[" +
                "{\"description\":\"fake1\"}" +
                ",{\"description\":\"fake2\"}]";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }

    @Test
    public void updateTestSituation() throws Exception {
        TherapyTest test = new TherapyTest("TestSituationUpdate");
        Situation situation = new Situation("Situation to update");
        situation.setId(1);
        test.addSituation(situation);

        when(mockedService.getSituationFromTest(anyString(), anyLong())).thenReturn(situation);
        when(mockedService.updateTestSituation(anyString(), any(Situation.class))).thenReturn(situation);

        String questionJson = getJsonFromSituation(situation);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/tests/" + test.getAnxietyType() + "/situations/" + situation.getId())
                .content(questionJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        JSONAssert.assertEquals(response.getContentAsString(), questionJson, false);
    }

    @Test
    public void deleteSituationFromTest() throws Exception {
        TherapyTest test = new TherapyTest("TestSituationDelete");
        Situation situation = new Situation("Situation to delete");
        situation.setId(1);
        test.addSituation(situation);

        when(mockedService.getSituationFromTest(anyString(), anyLong())).thenReturn(situation);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/tests/" + test.getAnxietyType() + "/situations/" + situation.getId());
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();
        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void addSituationToTest() throws Exception {
        Situation mockedSituation = new Situation("mocked");
        mockedSituation.setId(1);

        when(mockedService.addSituationToTest(any(String.class), any(Situation.class))).thenReturn(mockedSituation);
        String questionJson = "{\"description\":\"mocked\"}";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/tests/doesnt_matter/situations") // will be mocked, so wont have to be a real tests/id<-/situations
                .accept(MediaType.APPLICATION_JSON)
                .content(questionJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals("http://localhost/tests/doesnt_matter/situations/1", response.getHeader(HttpHeaders.LOCATION));
    }

    @Test
    public void getSituationFromTest() throws Exception {
        Situation mockedSituation = new Situation("mocked");
        mockedSituation.setId(1);

        when(mockedService.getSituationFromTest(any(String.class), any(Long.class))).thenReturn(mockedSituation);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/tests/whatever/situations/1")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expected = "{\"description\":\"mocked\"}";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }

    private String getJsonFromSituation(Situation situation){
        return "{\"id\":" + situation.getId() + ", \"description\":\"" + situation.getDescription() + "\"}";
    }

    private String getJsonFromSituations(Situation... situations){
        String json = "[";

        for (int i = 0; i<situations.length; i++){
            json = json + "{\"description\":\""+  situations[i].getDescription() +"\"}";
            i++;
            if(i<situations.length){
                json += ",";
            }
        }
        json = json + "]";
        return json;
    }

    private String getJsonFromTestObject(TherapyTest test){
        String json = "{\"anxietyType\":\""+ test.getAnxietyType() +"\"" +
                ", \"situations\":"
                + getJsonFromSituations(test.getSituations().toArray(new Situation[test.getSituations().size()]));

        json = json + "}";
        return json;
    }
}

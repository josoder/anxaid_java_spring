package se.josoder.anxaid.therapy;

import com.sun.javafx.collections.NonIterableChange;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import se.josoder.anxaid.Application;
import se.josoder.anxaid.model.therapy.test.Situation;
import se.josoder.anxaid.model.therapy.test.TherapyTest;
import se.josoder.anxaid.service.TherapyTestService;
import se.josoder.anxaid.util.HttpHelper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by josoder on 10.08.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TherapyTestRestControllerIT {
    private TestRestTemplate restTemplate;
    private HttpHeaders headers;

    @Autowired
    private TherapyTestService service;

    @Before
    public void init(){
        // auth
        headers = HttpHelper.createHttpHeaders("admin1", "secret1");
        restTemplate = new TestRestTemplate();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    }

    @LocalServerPort
    private int port;

    @Test
    public void retrieveAllTest(){
        String url = HttpHelper.getURL("/tests", port);

        ResponseEntity<List<TherapyTest>> response = restTemplate.exchange(url,
                HttpMethod.GET, new HttpEntity<String>("WhatEver", headers),
                new ParameterizedTypeReference<List<TherapyTest>>() {
                });

        TherapyTest test = new TherapyTest("test");

        assertTrue(response.getBody().contains(test));
    }

    @Test
    public void retrieveTestDetails(){
        String url = HttpHelper.getURL("/tests/test", port);

        ResponseEntity<TherapyTest> response = restTemplate.exchange(url,
                HttpMethod.GET, new HttpEntity<String>("WhatEver", headers),
                new ParameterizedTypeReference<TherapyTest>(){
                });

        TherapyTest test = new TherapyTest("test");

        assertTrue(response.getBody().equals(test));
    }

    @Test
    public void addTest() {
        String url = HttpHelper.getURL("/tests", port);

        TherapyTest test = new TherapyTest("addTest");

        HttpEntity entity = new HttpEntity<TherapyTest>(test, headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

        String actual = response.getHeaders().get(HttpHeaders.LOCATION).get(0);
        // Will redirect to the newly created test if successfully created.
        assertTrue(actual.contains("/tests/addTest"));
    }

    @Test
    public void retrieveAllSituationsFromTest() {
        TherapyTest test = new TherapyTest("faked");
        Situation situation1 = new Situation("fake1");
        Situation situation2 = new Situation("fake2");
        test.setSituations(new HashSet<>(
                Arrays.asList(
                        situation1,
                        situation2
                )
        ));

        service.addTherapyTest(test);

        String url = HttpHelper.getURL("/tests/faked/situations", port);

        ResponseEntity<List<Situation>> response = restTemplate.exchange(url,
                HttpMethod.GET, new HttpEntity<>("WhatEver", headers),
                new ParameterizedTypeReference<List<Situation>>(){
                });

        for(Situation s : response.getBody()){
            assertTrue(s.getDescription().equals(situation1.getDescription())
            || s.getDescription().equals(situation2.getDescription()));
        }
    }

}

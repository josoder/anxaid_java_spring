package se.josoder.anxaid.util;

import org.springframework.http.HttpHeaders;

import java.nio.charset.Charset;
import java.util.Base64;

/**
 * Created by josoder on 10.08.17.
 */
public class HttpHelper {
    public static HttpHeaders createHttpHeaders(String username, String password){
        HttpHeaders headers = new HttpHeaders();
        String auth = username + ":" + password;

        byte [] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));

        String headerValue = "Basic " + new String(encodedAuth);

        headers.add("Authorization", headerValue);

        return headers;
    }

    public static String getURL(String path, int port){
        return "http://localhost:" + port + path;
    }
}
